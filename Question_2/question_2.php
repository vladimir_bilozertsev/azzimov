<?php

ini_set('max_execution_time', 9000); //10800 seconds = 3 hours - my laptop is old(2 hours it makes for 100k)
// constants
$NUM_CHARS = 6;
$ARRAY_SIZE=1000;

//$CHARACTERS_SET = '0123456789';
$SYMBOLS='!@#$%^&*()_+=-";:/\|><?,.`';
$DIGITS = '0123456789';
$LOWERCASE = 'abcdefghijklmnopqrstuvwxyz';
$UPPERCASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

$CHARACTERS_SET = $DIGITS . $LOWERCASE . $UPPERCASE . $UPPERCASE .$SYMBOLS;
$CHARACTERS_LENGTH = strlen($CHARACTERS_SET);

function stringValid($str) {
    global $NUM_CHARS, $DIGITS, $LOWERCASE, $SYMBOLS;
   $contains_symbols = $contains_digit = $contains_lower = false;
    for($i=0; $i<$NUM_CHARS; $i++) {
        if($contains_digit && $contains_lower) {
            return true;
        }
        if(!$contains_digit && strpos($DIGITS, $str[$i]) !== false) {
            $contains_digit = true;
            continue;
        }
        if(!$contains_lower && strpos($LOWERCASE, $str[$i]) !== false) {
            $contains_lower = true;
            continue;
        }
        if(!$contains_lower && strpos($SYMBOLS, $str[$i]) !== false) {
            $contains_symbols = true;
            continue;
        }
    }
    if($contains_digit && $contains_lower && $contains_symbols) {
        return true;
    }
    return false;
}

function generateRandomString($length = 1) {
    global $CHARACTERS_SET, $CHARACTERS_LENGTH;
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $CHARACTERS_SET[mt_rand(0, $CHARACTERS_LENGTH - 1)];
    }
    if(stringValid($randomString)) {
        return $randomString;
    } else {
        return generateRandomString($length);
    }
}


function unBool($stringToCheck, $arr){
    global $NUM_CHARS;
    $foundDuplicateFlag = false;
    for($i=0; $i<count($arr); $i++){ //iterate through the array
        for($j=0; $j < $NUM_CHARS; $j++) { // iterate through the string
            if($arr[$i][$j] != $stringToCheck[$j]){
                break;
            }
            // if reached the last character, and it's a match
            // this means, we've got a duplicate!
            if($j == ($NUM_CHARS-1)) {
                $foundDuplicateFlag = true;
            }
        }
        if($foundDuplicateFlag) {
            // no reason to continue iterating throught the array any further, we've got that it's a duplicate.
            // need to re-generate
            break;
        }
    }
    if($foundDuplicateFlag) {
        // recursively
        return unBool(generateRandomString($NUM_CHARS), $arr);
    }
    // hurray, not a duplicate!
    return $stringToCheck;
}

$bigArray = new SplFixedArray($ARRAY_SIZE);

//count for big letters
$countBigLetters=null;


$mem_empty = memory_get_usage();

$start = microtime(false);
//
$startMemory = memory_get_usage();
//for($s = microtime(true), $i = 0; $i < $ARRAY_SIZE; $i++){
for($s = microtime(true),$i = 0; $i < $ARRAY_SIZE; $i++){
        $bigArray[$i] = unBool(generateRandomString($NUM_CHARS), $bigArray);
}
echo "SplArray(size): " .$ARRAY_SIZE."; creation time is ". (microtime(true) - $s) . PHP_EOL;
echo "<br/>Memory growth: ", memory_get_usage() - $startMemory, ' bytes'."<br />";

//echo memory_get_peak_usage(true)."<br />";
if(isset($_POST['filename']) && $_POST['filename']!=''){
    $f=$_POST['filename'];
     
$bigArray=json_encode($bigArray);
echo "Filename: ".$f."<br />";
echo "Saved! ";
file_put_contents('C:/'.$f.'.txt', $bigArray);
$f='';

}
else{
    echo "PLS, Enter filename!";
};


?>


